from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os,sys,inspect
import pymysql
from passlib.hash import sha256_crypt
import logging

# try:
#     os.environ["DOCKER"]
# except:
#     sys.exit(0)

# Importovanie modelov z parent dir
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
from models import *

# Getting the address from the enviroment
try:
    db_address = 'mysql+pymysql://{username}:{password}@{address}:3306/{database}'.format(address=os.environ["DB_ADDRESS"], username=os.environ["MYSQL_USER"], password=os.environ["MYSQL_PASSWORD"], database=os.environ["MYSQL_DATABASE"])
except:
    raise

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

engine = create_engine(db_address)
Base.metadata.create_all(engine)

Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

def insertDummyData():
    slovakia = Country(code='SVK', supported=True)
    keywords = [
        Keyword(value="herne notebooky", country=slovakia),
        Keyword(value="chladnicky", country=slovakia),
        Keyword(value="lacne auta", country=slovakia),
        Keyword(value="tunis dovolenka", country=slovakia),
    ]

    for kw in keywords:
        session.add(kw)

    firstUser = User(
            email="admin@invelity.com",
            name="Invelity Admin",
            password_hash = sha256_crypt.using(salt="specialnysalt").hash("admin")
        )
    
    session.add(firstUser)

    session.commit()

insertDummyData()