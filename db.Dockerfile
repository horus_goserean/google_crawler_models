FROM mysql:5.7

# Copy initial DB setup 
COPY ./Database/initial_setup.sql /docker-entrypoint-initdb.d/initial_setup.sql
