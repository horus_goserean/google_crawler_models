from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Table, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.ext.orderinglist import ordering_list
from sqlalchemy.ext.hybrid import hybrid_property


Base = declarative_base()

keyword_set_rel = Table('_rel_keyword_set_keyword', Base.metadata,
    Column('keyword_id', Integer, ForeignKey('keywords.id')),
    Column('keyword_set_id', Integer, ForeignKey('keyword_sets.id'))
)

user_keyword_rel = Table('_rel_user_keyword', Base.metadata,
    Column('keyword_id', Integer, ForeignKey('keywords.id')),
    Column('user_id', Integer, ForeignKey('users.id'))
)

user_keywordset_rel = Table('_rel_user_keywordset', Base.metadata,
    Column('keyword_set_id', Integer, ForeignKey('keyword_sets.id')),
    Column('user_id', Integer, ForeignKey('users.id'))
)

class Country(Base):
    __tablename__ = "countries"
    code = Column(String(3), primary_key=True, unique = True)
    vpn_ip = Column(String(50))
    vpn_username = Column(String(150))
    vpn_password = Column(String(150))
    supported = Column(Boolean)

    keywords = relationship('Keyword', back_populates='country')

class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, unique=True)
    email = Column(String(255), unique=True)
    name = Column(String(255))
    password_hash = Column(String(255))
    privilege = Column(Integer, default=1)

    keywords = relationship("Keyword", secondary=user_keyword_rel, back_populates="users")
    keyword_sets = relationship("KeywordSet", secondary=user_keywordset_rel, back_populates="users")

class Keyword(Base):
    __tablename__= 'keywords'
    id = Column(Integer, primary_key=True, unique=True)
    value = Column(String(255), nullable=False, unique=True)
    
    country_code = Column(String(3), ForeignKey("countries.code"))
    country = relationship("Country", back_populates="keywords")
    created_at = Column(DateTime)

    sets = relationship("KeywordSet", secondary=keyword_set_rel, back_populates="keywords")
    users = relationship("User", secondary=user_keyword_rel, back_populates="keywords")
    # One Keyword, many scans
    scans = relationship("Scan", back_populates="keyword",lazy="joined", order_by="Scan.performed_at", collection_class=ordering_list("performed_at"),cascade="delete, delete-orphan")

    def __repr__(self):
        return "Keyword(value=\"{0}\")>".format(self.value)

class KeywordSet(Base):
    __tablename__= 'keyword_sets'
    id = Column(Integer, primary_key=True, unique=True)
    name = Column(String(length=60))
    comment = Column(String(length=320))

    keywords = relationship("Keyword", secondary=keyword_set_rel, back_populates="sets")
    users = relationship("User", secondary=user_keywordset_rel, back_populates="keyword_sets")        


class Scan(Base):
    __tablename__ = "scans"
    id = Column(Integer, primary_key=True, unique=True)
    performed_at  = Column(DateTime)
    keyword_id = Column(Integer, ForeignKey("keywords.id"))

    # Scan - ads relationship
    ads = relationship("FoundAd", back_populates="scan",cascade="all, delete-orphan")
    # Keyword rel
    keyword = relationship("Keyword", back_populates="scans")

class FoundAd(Base):
    __tablename__ = "found_ads"
    id = Column(Integer, primary_key=True, unique=True)
    position = Column(Integer)
    scan_id = Column(Integer, ForeignKey("scans.id"))
    website_id = Column(Integer, ForeignKey("websites.id"))

    scan = relationship("Scan", back_populates="ads")
    website = relationship("Website", back_populates="found_ads")

    @hybrid_property
    def score(self):
        def calculateScore(position, ofTotal):
            return float("{0:.2f}".format(((ofTotal-(position)+1)*100)/ofTotal))
        
        return calculateScore(self.position+1, len(self.scan.ads))


class Website(Base):
    __tablename__ = "websites"
    id = Column(Integer, primary_key=True, unique=True)
    domain = Column(String(255), unique = True)
    logo_url = Column(String(255))

    owner_id = Column(Integer, ForeignKey("owners.id"))
    owner = relationship("Owner", back_populates="websites")
    found_ads = relationship("FoundAd", back_populates="website")

class Owner(Base):
    __tablename__ = "owners"
    id = Column(Integer, primary_key =True, unique = True)
    name = Column(String(255))
    comment = Column(String(320))

    websites = relationship("Website", back_populates="owner")