import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="google_crawler_models",
    version="1.3.1",
    author="Jákob Rolík",
    author_email="rolik.jakob@gmail.com",
    description="Private package for the Google Crawler project",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/invelity/emge/invelity-google-crawler",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache License 2.0",
        "Operating System :: OS Independent",
    ),
)
